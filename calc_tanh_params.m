function [e, e_g, u_g, u0] = calc_tanh_params(P, D_i, D_o, a, b, free, mode)
% given experimentally measurable quantities, calculate tanh parameters
% free is either u0 (symmetric case) or phi_i (asymmetric case)

%   Symmetric around u0
if strcmp(mode, 'symmetric')
    u0 = free;
    e = u0*(P-1)/(P+1);
    phi_tot_in = Ternary_model.phi_tot(-1000, a, b, e, u0, 0);
    phi_tot_out = Ternary_model.phi_tot(1000, a, b, e, u0, 0);
    g0_in = D_i/(1-phi_tot_in);
    g0_out = D_o/(1-phi_tot_out);
    e_g = g0_out-g0_in;
    u_g = g0_in;

%   Phi_tot in given.
elseif strcmp(mode, 'asymmetric')
    phi_tot_in = free;
    phi_tot_out = phi_tot_in/P;
    g0_in = D_i/(1-phi_tot_in);
    g0_out = D_o/(1-phi_tot_out);
    e_g = g0_out-g0_in;
    u_g = g0_in;
    u0 = phi_tot_in/2*(1+1/P);
    e = phi_tot_in-u0;
end
end