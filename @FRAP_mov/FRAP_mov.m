classdef FRAP_mov < handle
    properties
        bc_exp
        bleach_remainder % Fluorescence after bleach inside drop
        camera_bg % camera background with no sample.
        conv_factor_D
        edge_correction = 1;
        edge_ind; % pixel index of boundary
        exitflag; % for fminsearch trouble shooting
        f_min
        fval; % cost function value after optimization
        frap_frame % Last frame before bleach
        geometry; % 2: spherical, 1: cylindrical, 0: slab (see pdepde)
        i_exp
        i_exp_no_avg
        l_t
        model; % model for fitting D_in, usually 'simple_drop'
        model_data; % keeps data from full model, e.g. fitted D_out etc.
        name;
        norm_fac;
        output; % for fminsearch trouble shooting
        pixel_size;
        post_frame; % Frame after bleach at which analysis starts
        pre_bleach;
        pre_profile;
        rad;
        skip_nth;
        time_step;
        t;
        ti;
        v_fit;
        v_fit_end;
        x; % radial distance
        x_fit;
        x_seed = 1;
    end
    
    methods
        function M = FRAP_mov(name, pixel_size, rad, skip_nth, frap_frame,...
                                l_t, post_frame, model, edge_correction,...
                                camera_bg, geometry)
            if nargin > 0
                M.edge_correction = edge_correction;
                M.frap_frame = frap_frame;
                M.l_t = l_t;
                M.model = model;
                M.name = name;
                M.pixel_size = pixel_size;
                M.post_frame = post_frame;
                M.rad = rad;
                M.skip_nth = skip_nth;
                M.camera_bg = camera_bg;
                M.geometry = geometry;
                M.analyse();
            end
        end
        
        analyse(M)
        calc_Sim(M)
        fit_drop(M)
        plot_movie(M, nth, norm, movie)
        radial_profile(M)
        sim_profile(M)
    end
end