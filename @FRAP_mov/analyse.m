function analyse(M)
% ANALYSE(M) gets time steps for experimental data, creates radial average
% and fits data to M.model. Alternatively, if input is simulation read
% equivalent parameters.
if isnumeric(M.edge_correction)
    M.ti = analyse_image([M.name, '_a.tif']);
    if isnan(M.l_t); M.l_t = size(M.ti, 3) - M.post_frame; end
    try
        M.pre_bleach = analyse_image([M.name, '_prebleach.tif']);
        M.pre_bleach = mean(M.pre_bleach(:, :, :), 3);
    catch
        disp(['No prebleach provided, arbitrary normalization, ', ...
               'no edge correction possible. If edge_corr == 1, set to 0.']);
        if M.edge_correction == 1; M.edge_correction = 0; end
    end
    [M.t, M.time_step] = get_time_steps(['Meta_', M.name],...
                                    M.frap_frame, M.l_t, M.post_frame);
    M.radial_profile();
elseif strcmp(M.edge_correction, 'Sim')
    M.sim_profile();
end

% Calculate conversion factor to get D with units.
si_i = size(M.i_exp);
if ~isnan(M.time_step)
    end_time = M.time_step*si_i(1);
else
    end_time = M.t(end);
end
M.x = M.pixel_size*(0:si_i(2)-1);
M.conv_factor_D = (M.pixel_size*si_i(2))^2/(end_time);
M.fit_drop();
M.calc_Sim();
end