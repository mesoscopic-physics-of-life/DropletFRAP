function plot_movie(M, nth, norm, movie)
% PLOT_MOVIE plots movie of fit and data, can pause at each frame.
blue = [0    0.4470    0.7410];
yell = [255/255, 217/255, 128/255];

si_v = size(M.v_fit);
x = M.pixel_size*(0:si_v(2)-1);

i_exp = M.i_exp(1:nth:si_v(1), :);
v_fit = M.v_fit(1:nth:si_v(1), :);
ylabel = 'intensity I (a.u.)';

if norm
    i_exp = i_exp./v_fit(:, end);
    v_fit = v_fit./v_fit(:, end);
    ylabel = 'I / I_{boundary}';
end

figure(23); hold on;
if movie == 0
    make_graph_pretty(['r [' char(956) 'm]'], 'Intensity [a.u.]', '',...
                       [0, max(x), 0, 1.1*max(v_fit(:))]);
    plot(x, i_exp', 'Color', yell, 'LineWidth', 3);
    plot(x, v_fit', '--', 'LineWidth', 4, 'Color', blue);
elseif movie==1
    for i = 1:size(v_fit, 1)
        clf; hold on; 
        plot(x, i_exp(i, :)', 'LineWidth', 3);
        plot(x, v_fit(i, :)', '--', 'LineWidth', 3);
        make_graph_pretty(['r [' char(956) 'm]'], ylabel, '',...
                          [0, max(x), 0, 1.1*max(v_fit(:))]);
        pause();
    end
elseif movie == 2
    plot(M.t, sum(M.i_exp, 2), 'LineWidth', 2, 'Color', yell);
    plot(M.t, sum(M.v_fit, 2), 'LineWidth', 2, 'Color', blue);
    make_graph_pretty('time (s)', 'intensity (a.u.)', '',...
        [0, max(M.t), 0, inf]);
end
end