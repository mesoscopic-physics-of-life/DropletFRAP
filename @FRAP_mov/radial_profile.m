function radial_profile(M)
si = size(M.ti);
pre_profile = radialavg(M.pre_bleach, floor(min([si(1), si(2)])/2));
i_exp = zeros(si(3), length(pre_profile));
i_exp_no_avg = zeros(si(3), length(pre_profile));
ind = zeros(1, si(3));

for i = 1:si(3)
    row_m = floor(si(1)/2);
    col_m = floor(si(2)/2);
    i_exp_no_avg(i, :) = mean(M.ti(row_m-2:row_m+2,...
                                     col_m:col_m + length(pre_profile)...
                                     - 1 , i));
    i_exp(i, :) = radialavg(M.ti(:, :, i), floor(min([si(1), si(2)])/2));
    [~, ind(i)] = min(diff(sgolayfilt(i_exp(i, :), 3, 11)));%%15
end

% Cut off nans if size of image is even (e.g. 70*70 instead of 71*71)
if isnan(i_exp(1))
    i_exp = i_exp(:, 2:end);
    pre_profile = pre_profile(2:end);
end

% Get normalization - total intensity prior / total intensity @ end
M.norm_fac = mean(pre_profile(M.rad-3:M.rad)-M.camera_bg)/...
             mean(i_exp(end, M.rad-3:M.rad)-M.camera_bg);

% Get full length prebleach profile, subtract background
bg = M.camera_bg;
pre_profile = sgolayfilt(pre_profile - bg, 3, 5);
[~, M.edge_ind] = min(diff(pre_profile));


% Filter and interpolate/extrapolate indices not found for droplet boundary
% Depending on how well this works (S/N etc.) this can introduce
% significant fluctuations at the outer boundary.

% subtract background
i_exp = i_exp - bg;
i_exp_no_avg = i_exp_no_avg - bg;

% Get postbleach background, remainder after bleach
i_exp_s = sort(i_exp(1, :));
M.bleach_remainder = mean(i_exp_s(1:4));

if M.edge_correction == 1
    ind(ind<si(1)/5) = nan;
    ind = fillmissing(ind, 'nearest');
    ind(ind<M.edge_ind) = M.edge_ind;
    ind = round(sgolayfilt(ind, 2,11)); % To avoid excessive jumping at boundary

    siz = size(i_exp);
    bg_indiv = mean(i_exp(:, end-5:end), 2);
    bg_indiv = repmat(bg_indiv, 1, siz(2));
    r_fit_exp_above_bulk = i_exp - bg_indiv;

    % Correct r_fit_exp for rounding off at the edge. Due to the movement of
    % the droplet boundary (droplet growth), every frame is corrected with a
    % slightly stretched version of the pre_bleach_profile. Amount of stretch
    % determined by finding steepest point of each profile and comparing to pb.
    norm_arr = zeros(si(3), M.rad);
    for i = 1:si(3)
        y1 = interp1(1:M.edge_ind, pre_profile(1:M.edge_ind),...
                     linspace(1, M.edge_ind, ind(i)));
        if length(y1) < 15; len = ceil(length(y1)/2); else; len = 10; end
        norm_arr(i, :) = y1(1:M.rad)/mean(y1(1:len)); % Cut to length = rad.
    end

    i_exp = r_fit_exp_above_bulk(:, 1:M.rad)./norm_arr; % Apply correction
    i_exp_no_avg = i_exp_no_avg(:, 1:M.rad)./norm_arr; % Apply correction

    % Re-add bulk concentration above initial zero of each individual profile.
    i_exp = i_exp + bg_indiv(:, 1:M.rad);
elseif M.edge_correction == 0 
    % No edge_correction
    i_exp = i_exp(:, 1:M.rad);
    i_exp_no_avg = i_exp_no_avg(:, 1:M.rad);
elseif M.edge_correction == 2
    % Edge correction by last frame, this is somewhat approximate and needs
    % to be used with care, examine every single movie for saturation!
    i_exp = i_exp(:, 1:M.rad);
    i_exp = (i_exp'./i_exp(end, :)')';
elseif M.edge_correction == 3
    % No edge correction, just stretching, to tentatively adjust for
    % changes in focus or droplet growth/shrinkage if no prebleach is
    % known. Stretch by position of maximum.
    % --- Not implemented for now ---
end

% Normalize experimental input to make independent of fluorescence
% intensity, which is a.u. anyway. This needs to be done before initial
% conditions are assigned for simple drop.
if ~isnan(M.norm_fac)
    M.i_exp_no_avg = i_exp_no_avg/mean(i_exp(end, end-3:end))/M.norm_fac;
    M.i_exp = i_exp/mean(i_exp(end, end-3:end))/M.norm_fac;
else
    M.i_exp_no_avg = i_exp_no_avg;
    M.i_exp = i_exp;
end
% Get values for experimental boundary conditions
M.bc_exp = sort(sgolayfilt(M.i_exp(:, end), 2, 5));
% Assign pre_profile
M.pre_profile = pre_profile;
% Cut all arrays to desired size
M.bc_exp = M.bc_exp(M.post_frame:M.skip_nth:M.post_frame + M.l_t);
M.i_exp_no_avg = M.i_exp_no_avg(M.post_frame:M.skip_nth:M.post_frame + M.l_t, :);
M.i_exp = M.i_exp(M.post_frame:M.skip_nth:M.post_frame + M.l_t, :);
M.ti = M.ti(:, :, M.post_frame:M.skip_nth:M.post_frame + M.l_t);
% Conversion factor for D to have units of mum^2/s.
