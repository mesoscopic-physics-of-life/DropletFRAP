function sim_profile(M)
% SIM_PROFILE Get simulation meta data and read in profiles if
% M.edge_correction=='Sim'
M.i_exp = readmatrix(['t_p_', M.name, '.csv']);
M.bc_exp = M.i_exp(:, end);
meta_data = readmatrix(['meta_data_', M.name, '.csv']);
M.time_step = meta_data(1);
M.t = linspace(0, M.time_step*meta_data(2), meta_data(2))';
% Here's the problem with t. zero in front makes for better agreement. why?
M.norm_fac = 1;
M.pixel_size = mean(diff(meta_data(3:end-1)));
M.edge_ind = meta_data(end);
end