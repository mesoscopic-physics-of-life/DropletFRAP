function fit_drop(M)
% Fitting routine.
f_min = @(x) to_minimize(M.i_exp, M.t, x, M.model, M.bc_exp,...
                         M.pixel_size, M.geometry);
opt = optimset('MaxFunEvals', 2000, 'PlotFcns',@optimplotfval,...
               'TolX', 1e-06, 'TolFun', 1e-06);
% Could use matlabcentral's fminsearchbnd-fminsearchcon
% instead of fminsearch if bound problem is really needed.
[M.x_seed, M.fval, M.exitflag, M.output] = fminsearch(f_min, M.x_seed, opt);
end