function calc_Sim(M)
% CALC_SIM calculate fit and normalized experimental intensities

[M.fval, M.v_fit, M.v_fit_end, ~, ~, M.x_fit] =...
      to_minimize(M.i_exp, M.t, M.x_seed, M.model, M.bc_exp, M.pixel_size,...
                  M.geometry);
end