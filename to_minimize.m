function [cost, v_fit, v_fit_end, v_fit_1, v_fit_2, x_fit] = ...
                                to_minimize(r_n, t, x_seed, model, bc_data,...
                                            pixel_size, geometry)
% TO_MINIMZE cost function for fitting diffusion/binding models to 
% experimental data with boundary flux proportional to difference between 
% outside and inside. 
% extrp - number of time points to be extrapolated beyond the provided data

% Create space and time domains to be compared
si = size(r_n);
if ~strcmp(model, 'full_drop')
    % t = [linspace(0, 1, si(1)), 15];
    t = [t./max(t); 15];
end
x_fit = linspace(0, 1, si(2));
   

if strcmp(model, 'simple_drop')
    % If the profile is not flat initially, a time delay needs to be
    % introduced, because pdepe always solves from 0, therefore neglecting
    % any potential time that has elapsed since the frap experiment. This
    % delay can introduce a shift, mitigating effects of the initial
    % conditions, use with caution, always compare to simply starting from
    % the experimental IC, without delay.
    if length(x_seed) == 3
        t = [-abs(x_seed(3)), t];
    end
    
    % Compute solution of model, compare to experimental input r_n
    if length(x_seed) == 1
        t = t(1:end-1); % t cannot be extrapolated if exp. BCs are used
        v_fit = simple_drop(x_fit, t, abs(x_seed(1)), abs(x_seed(1)),...
                            geometry, x_fit, r_n(1, :), t, bc_data);
        v_fit_end = v_fit(end, :);
    else
        v_fit = simple_drop(x_fit, t, abs(x_seed(2)), abs(x_seed(1)),...
                            geometry, x_fit, r_n(1, :));
        v_fit_end = v_fit(end, :);
        v_fit = v_fit(1:end-1, :);
    end
    
    % If a time delay is used, this needs to be cut off, thus arrays starts
    % at 2: .
    if length(x_seed)==3
        v_fit = v_fit(2:end,:);
    end
    v_fit_1 = [];
    v_fit_2 = [];
elseif strcmp(model, 'full_drop')
    % Use the full model, including bulk.
    T = Ternary_model(2, 'FRAP', [-1, 0.05, 0.5, 0.4, x_seed(1), x_seed(2),...
        40], t, 1);
    T.solve_tern_frap();
    v_fit_end = v_fit(end, :);
    v_fit = v_fit(1:end, :);
    v_fit_1 = [];
    v_fit_2 = [];
elseif strcmp(model, 'gel_drop')
    % Explanation see simple_drop
    if length(x_seed) == 5
        t = [-abs(x_seed(5)), t];
    end
    
    % Compute solution of pde
    v_fit = gel_drop(x_fit, t, abs(x_seed(2)), abs(x_seed(1)), abs(x_seed(3)),...
        abs(x_seed(4)));
    v_fit_end = sum(v_fit(end, :, :), 3)/(1+x_seed(4)/x_seed(3));
    v_fit = v_fit(1:end-1, :, :)/(1+x_seed(4)/x_seed(3));
    
    % Explanation see simple_drop
    if length(x_seed)==5
        v_fit = v_fit(2:end, :, 1) + v_fit(2:end, :, 2);
        v_fit_1 = v_fit(2:end, :, 1);
        v_fit_2 = v_fit(2:end, :, 2);
    else
        v_fit_1 = v_fit(:, :, 1);
        v_fit_2 = v_fit(:, :, 2);
        v_fit = v_fit(:,:,1) + v_fit(:,:,2);
    end
    
elseif strcmp(model, 'two_fluid_drop')    
    if length(x_seed) == 6
        t = [-abs(x_seed(6)), t];
    end
    % The two_fluid_drop model assumes two independent fluids that can
    % exchange with the bulk and recovery according to simple_drop,
    % however, with independent parameters.
    v_fit_1 = simple_drop(x_fit, t, abs(x_seed(2)), abs(x_seed(1)), geometry);
    v_fit_2 = simple_drop(x_fit, t, abs(x_seed(4)), abs(x_seed(3)), geometry);
    v_fit_1 = x_seed(5)*v_fit_1;
    v_fit_2 = (1-x_seed(5))*v_fit_2;
    v_fit = v_fit_1 + v_fit_2;
    v_fit_end = v_fit(end, :);
    v_fit = v_fit(1:end-1, :);
    % If a time delay is used, this needs to be cut off, thus arrays starts
    % at 2: .
    if length(x_seed)==6
        v_fit = v_fit(2:end, :);
    end

elseif strcmp(model, 'agg_drop') 
    %%%%% ATTENTION THIS IS NOT CURATED ANYMORE, CHECK BEFORE USE.%%%%%%
    
    N_max = 200; % Maximum number of monomers per cluster
    M = 1:N_max; % mass of particles as multiple of monomer mass = 1
    
    % Set distribution of cluster sizes according to constant kernel
%     c = @(t, k) (t.^(k-1))./((1+t).^(k+1));
%     cs = M.*c(1, M);
    % Use two gaussians:
    c_double_gauss = @(x, sig1, sig2, mu1, mu2, A1, A2) ...
                A1/(sig1*sqrt(2*pi))*exp(-(x-mu1).^2/(2*sig1^2)) + ...
                A2/(sig2*sqrt(2*pi))*exp(-(x-mu2).^2/(2*sig2^2));
%     cs = c_double_gauss(linspace(0, 1, N_max), 0.01, 0.01, 0.02,...
%                           0.97, 1, 1);
    % Uniform distribution
    cs = ones(1, N_max)/N_max;
    % Normalize cs 
    cs = num2cell(cs/nansum(cs));
    
    % Diff coefficient of particles via Einstein relation
%     D = M.^(-1/3); 
    % Diff coefficients are have bimodal distro
    D = c_double_gauss(linspace(0, 1, N_max), 0.05, 0.05, 0.05, 0.95,...
                        0.1, 0.1);
    % Step size diffusion:
    D = heaviside(linspace(-1, 1, N_max))+0.01;
    D = ones(1, N_max);
    D = D/max(D);
    % Now, instead of solving pde N_max times, it appears much faster to
    % scale time according to D and interpolate the solution accordingly.
    % This assumes the flux to scale with D.
    t_scaling = 1./sqrt(D); % time scaling, higher D make time go faster.
    if max(t_scaling > 5)
        disp('Check out t_scaling, precision lost due to cluster sizes');
    end
    t_mat = repmat(t, N_max, 1);
    t_all = (t_mat.*repmat(t_scaling', 1, si(1)))'; % scale time
    t_all = t_all(:);
    [s, indx] = sort(t_all); % sort, to obtain monotonous time for pdepe
    [s_unique, ~, IC] = unique(s);
    v_fit = simple_drop(x_fit, s_unique, abs(x_seed(2)), abs(x_seed(1)),...
                        geometry);
    a = v_fit(IC, :);
    unsorted = 1:length(t_all);
    newInd(indx) = unsorted;
    b = a(newInd, :);   

    b_cells = mat2cell(b, ones(1, N_max)*si(1), si(2));
    b_cells_norm = cellfun(@(x, y) x*y, b_cells, cs', 'Uni', 0);
    test = reshape(cell2mat(b_cells_norm'), si(1), si(2), N_max);
    v_fit = nansum(test, 3);
    v_fit_1 = [];
    v_fit_2 = [];
    
end

% Normalize v_fit to make independent of total intensity, which is
% arbitrarily set by the boundary conditions in each model
% v_fit_end = v_fit_end/mean(v_fit(end, 10:end));
% v_fit = v_fit/mean(v_fit(end, 10:end));

% Return cost as the sum of squared differences of model and experiment
[N, ~, B] = histcounts(mean(r_n, 2), 20);
% if 1
%     cd('~/Desktop/DropletFRAP/PSF/');
%     psf = load_stack('MCZ120211124_35616_SingleDM-4.tif');
%     psf = psf/max(psf, [], 'all')-100;
%     xy_res = pixel_size;
%     z_res = 0.28;
%     droplet_size = si(2)*pixel_size;
%     x = -droplet_size:xy_res: droplet_size;
%     y = x;
%     z = -droplet_size:z_res:droplet_size;
%     [X, Y, Z] = meshgrid(x, y, z);
%     dist = sqrt((X.^2+Y.^2+Z.^2))/droplet_size;
%     int = 50*ones(si);
%     for i = 1:si(1)
%         int = interp1(x_fit, v_fit(i, :), dist, 'linear', 50);
%         int_con = convn(int, psf.^2, 'same');
%         v_fit(i, :) = int_con(40, end-38:end, 19);
%     end
%     
% end
cost = sum(sum((v_fit-r_n).^2, 2)./N(B)'./mean(r_n, 2))/numel(v_fit);
% cost = sum(sum((v_fit(:, end-7:end)-r_n(:, end-7:end)).^2, 2)./N(B)'./mean(r_n, 2))/numel(v_fit);
end
