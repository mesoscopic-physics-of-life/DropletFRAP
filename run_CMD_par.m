%% CMD
addpath(genpath('/home/hubatsch/frap_theory'));
pa = '/data/biophys/hubatsch/MatlabWorkspaces/';
load([pa, 'DiffusionMeasurements/Coacervates.mat'], 'f_CMD')
%%
f_temp = f_CMD;
fixed = {'D_i', 'P'};
fit = { 'D_o', 'off'};
P = logspace(0, 2.5, 12);
prec = [5, 5, 5, 5, 5, 5, 10, 10, 10, 10, 15, 15];
fix_params = {nan, P};
fix_params = combvec(fix_params{:});
fit_seed = ones(2, length(P)*length(f_temp));
fit_seed(1, :) = repmat(0.04*P, 1, length(f_temp));
skip = 1:9;
tic
[fval, fit_seed, n_f, fix_params] = ...
  run_fits(f_temp, fixed, fix_params, fit, fit_seed, skip, 'droplet', prec);
toc
save([pa, 'Partitioning_vs_Dout/CMD_200221.mat']);