// clean up first
run("Close All");
run("Clear Results");
if (roiManager("count") > 0) {
	roiManager("deselect");
	roiManager("delete");
}

//open("/Users/hubatsch/Desktop/DataLars_putbackonserver/21_09_21/RPS-7_FAM_2/tseries1020210921_50503_reg_cro_a.tif");
open("/Users/hubatsch/Desktop/DataLars_putbackonserver/21_09_21/RPS-15_FRET_2/tseries1120210921_reg_a.tif");
//run("Duplicate...", " ");
run("Duplicate...", "duplicate range=20-20");
run("Invert LUT");
input = getTitle();
run("CLIJ2 Macro Extensions", "cl_device=Radeon");
Ext.CLIJ2_clear();
Ext.CLIJ2_getGPUProperties(gpu, memory, opencl_version);

// push data to GPU
Ext.CLIJ2_push(input);
Ext.CLIJ2_gaussianBlur2D(input, blurred, 10, 10);
Ext.CLIJ2_thresholdOtsu(blurred, mask);
Ext.CLIJ2_connectedComponentsLabelingBox(mask, labelmap);

// show the image
Ext.CLIJ2_pull(input);
// show label as ROIs on top of the image
Ext.CLIJ2_pullLabelsToROIManager(labelmap);
roiManager("show all");
run("Set Measurements...", "centroid area redirect=None decimal=3");
numROIs = roiManager("count");
largest = 0;
for(i=0; i<numROIs;i++) {// loop through ROIs
	roiManager("Select", i);
	run("Measure");
	A = getResult("Area", i);
	if (A>largest) {
		largest = A;
		x = getResult("X", i);
		y = getResult("Y", i);
	}
}
print(largest);
print(x);
Ext.CLIJ2_resliceRadial(input, resliced, 360, 1, 0, x, y, 1, 1);
Ext.CLIJ2_pull(resliced);

//Roi.getCoordinates(x, y);