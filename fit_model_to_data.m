out_path = '~/Desktop/DropletFRAP/Latex/Figures/Fig4/';
pa = '/data/biophys/hubatsch/MatlabWorkspaces/';
load_path = '~/Desktop/DropletFRAP/MatlabWorkspaces/Partitioning_vs_Dout/';
% Space rescaling:
d_fac = @(f) cellfun(@(x) (x.pixel_size*size(x.i_exp, 2))^2, f);
%% Write out csv for python
load([load_path, 'ATP_200221.mat']);
D_out_PLYS = d_fac(f_temp).*reshape(fit_seed(1, :), length(P), length(f_temp));
PLYS = table(repmat(P', length(f_temp), 1), D_out_PLYS(:),...
            'VariableNames', {'P', 'D_out'});
writetable(PLYS, [out_path, 'PLYS.csv'], 'WriteVariableNames', 1)
%% Test individual simulation
% jj = find(n_f==5 & fix_params(3, :)'==2);
jj = 6;
prec_r = repmat(prec, 1, length(f_temp)); 
D = abs(f_temp{n_f(jj)}.conv_factor_D*f_temp{n_f(jj)}.x_seed);
[cost, T] = to_min(fit_seed(:, jj), fit, [D; fix_params(2, jj)], fixed,...
                   f_temp{n_f(jj)}, 'droplet', prec_r(jj));
%% PLYS time course
x_scale = f_temp{n_f(jj)}.pixel_size*size(f_temp{n_f(jj)}.i_exp, 2);
si_v = size(f_temp{n_f(jj)}.v_fit);
x = f_temp{n_f(jj)}.pixel_size*(0:si_v(2)-1);
writetable(table(x', f_temp{n_f(jj)}.i_exp(:, :)'),...
           [out_path, 'PLYS_timecourse.csv'], 'WriteVariableNames', 0);
writetable(table(T.x'*x_scale, T.sol(:, :)'/T.phi_t(1)),...
           [out_path, 'PLYS_fit_timecourse.csv'], 'WriteVariableNames', 0); 
csvwrite([out_path, 'PLYS_fit_time.csv'], T.t); 
%% CMD, write out csv for python
load([load_path, 'CMD_200221.mat']);
D_out_CMD = d_fac(f_CMD).*reshape(fit_seed(1, :), length(P), length(f_CMD));
CMD = table(repmat(P', length(f_CMD), 1),  D_out_CMD(:),...
            'VariableNames', {'P', 'D_out'});
writetable(CMD, [out_path, 'CMD.csv'], 'WriteVariableNames', 1)
%% Test individual simulation
jj = 31;
prec_r = repmat(prec, 1, length(f_temp)); 
D = abs(f_temp{n_f(jj)}.conv_factor_D*f_temp{n_f(jj)}.x_seed);
[cost, T] = to_min(fit_seed(:, jj), fit, [D; fix_params(2, jj)], fixed,...
                   f_temp{n_f(jj)}, 'droplet', prec_r(jj));
%% CMD time course
x_scale = f{n_f(jj)}.pixel_size*size(f_temp{n_f(jj)}.i_exp, 2);
si_v = size(f_temp{n_f(jj)}.v_fit);
x = f_temp{n_f(jj)}.pixel_size*(0:si_v(2)-1);
writetable(table(x', f_temp{n_f(jj)}.i_exp(1:end, :)'),...
           [out_path, 'CMD_timecourse.csv'], 'WriteVariableNames', 0);
writetable(table(T.x'*x_scale, T.sol([1, 2:end], :)'/T.phi_t(1)),...
           [out_path, 'CMD_fit_timecourse.csv'], 'WriteVariableNames', 0); 
csvwrite([out_path, 'CMD_fit_time.csv'], T.t);
%% PGL by salts
load([load_path, 'PGL_P_vs_D_cluster_200226.mat']);
figure; hold on;
D_temp = d_fac(f).*reshape(fit_seed(1, :), length(P), length(f));
plot(P, mean(D_temp(:, 12:14), 2), 'k--');
plot(P, mean(D_temp(:, 15:17), 2), 'o');
plot(P, mean(D_temp(:,18:21), 2), 'gx');
plot(P, mean(D_temp(:, 22:27), 2), 'ko');
plot(P, mean(D_temp(:, 5:7), 2), 'm.-');
plot(P, mean([D_temp(:, 1:2), D_temp(:, 11)], 2), 'r.-');
plot(P, mean(D_temp(:, 8:10), 2), 'b');
plot(P, mean(D_temp(:, 3:4), 2), 'm');
ax = gca();
ax.XScale = 'log';
ax.YScale = 'log';
%% Test individual simulation
jj = 6;
prec_r = repmat(prec, 1, length(f_temp)); 
D = abs(f_temp{n_f(jj)}.conv_factor_D*f_temp{n_f(jj)}.x_seed);
[cost, T] = to_min(fit_seed(:, jj), fit_s, [D; fix_params(2, jj)], fix_s,...
                   f_temp{n_f(jj)}, 'droplet', prec_r(jj));
%% PGL time course
x_scale = f{n_f(jj)}.pixel_size*size(f_temp{n_f(jj)}.i_exp, 2);
si_v = size(f_temp{n_f(jj)}.v_fit);
x = f_temp{n_f(jj)}.pixel_size*(0:si_v(2)-1);
writetable(table(x', f_temp{n_f(jj)}.i_exp(1:end, :)'),...
           [out_path, 'PGL_timecourse.csv'], 'WriteVariableNames', 0);
writetable(table(T.x'*x_scale, T.sol([1, 2:end], :)'/T.phi_t(1)),...
           [out_path, 'PGL_fit_timecourse.csv'], 'WriteVariableNames', 0);
csvwrite([out_path, 'PGL_fit_time.csv'], T.t);
%% Write out csv for python
PGL_50 = table(repmat(P', 2, 1), ...
               reshape(D_temp(:, 3:4),numel(D_temp(:, 3:4)), 1),...
                'VariableNames', {'P', 'D_out'});
writetable(PGL_50, [out_path, 'PGL_50.csv'], 'WriteVariableNames', 1)
PGL_60 = table(repmat(P', 3, 1), ...
               reshape(D_temp(:, 8:10),numel(D_temp(:, 8:10)), 1),...
                'VariableNames', {'P', 'D_out'});
writetable(PGL_60, [out_path, 'PGL_60.csv'], 'WriteVariableNames', 1)
PGL_75 = table(repmat(P', 3, 1), ...
               reshape([D_temp(:, 1:2), D_temp(:, 11)],...
               numel([D_temp(:, 1:2), D_temp(:, 11)]), 1),...
                'VariableNames', {'P', 'D_out'});
writetable(PGL_75, [out_path, 'PGL_75.csv'], 'WriteVariableNames', 1)

PGL_90 = table(repmat(P', 3, 1), ...
               reshape(D_temp(:, 5:7),numel(D_temp(:, 5:7)), 1),...
                'VariableNames', {'P', 'D_out'});
writetable(PGL_90, [out_path, 'PGL_90.csv'], 'WriteVariableNames', 1)
PGL_100 = table(repmat(P', 6, 1), ...
               reshape(D_temp(:, 22:27),numel(D_temp(:, 22:27)), 1),...
                'VariableNames', {'P', 'D_out'});
writetable(PGL_100, [out_path, 'PGL_100.csv'], 'WriteVariableNames', 1)
PGL_120 = table(repmat(P', 4, 1), ...
               reshape(D_temp(:,18:21),numel(D_temp(:,18:21)), 1),...
                'VariableNames', {'P', 'D_out'});
writetable(PGL_120, [out_path, 'PGL_120.csv'], 'WriteVariableNames', 1)
PGL_150 = table(repmat(P', 3, 1), ...
               reshape(D_temp(:, 15:17),numel(D_temp(:, 15:17)), 1),...
                'VariableNames', {'P', 'D_out'});
writetable(PGL_150, [out_path, 'PGL_150.csv'], 'WriteVariableNames', 1)
PGL_180 = table(repmat(P', 3, 1), ...
               reshape(D_temp(:, 12:14),numel(D_temp(:, 12:14)), 1),...
                'VariableNames', {'P', 'D_out'});
writetable(PGL_180, [out_path, 'PGL_180.csv'], 'WriteVariableNames', 1)

%% reshaped
P_r = reshape(P, length(unique(P)), length(f_temp));
D_r = reshape(D_out, length(unique(P)), length(f_temp));
% off_r = reshape(off, length(unique(P)), length(f_temp));
fval_r = reshape(fval, length(unique(P)), length(f_temp));              
%% 
T.plot_sim('plot', 25, 'r', 2, x_scale)
f_temp{n_f(jj)}.plot_movie(25, 0, 0)
axis([0, 15, 0, 0.5])
%% Get any curve with large jumps
[~, inds] = max(diff(fval_r), [], 2);
inds = unique(inds);
f_temp = f1(inds);
D_start = repmat(mean(D_r(:, inds_good), 2), length(f_temp), 1);
off_s = repmat(mean(off_r(:, inds_good), 2), length(f_temp), 1);
% inds_good = 4:9;
% [~, inds] = min(fval_r(:, inds_good), [], 1);
%% Fit simulation to data
b = 2e-05;
free = 0.5;
t_sim = linspace(0, 50, 10);
P = [5, 150, 5, 150];
D_o = [0.1, 0.1, 1, 1];
T = {};
parfor i = 1:length(D_o)
    [e, e_g, u_g, u0] = calc_tanh_params(P(i), 0.01, D_o(i), -1, b, free,...
                                     'asymmetric');
    off = 0.08*exp(-abs(1000));
    T{i} = Ternary_model(2, 'FRAP', {-1, b, u0, e, e_g, u_g, 100, 0, 0,...
                                  'Constituent', off}, t_sim, 60);
    T{i}.solve_tern_frap();
end
%%
fit_s = {'D_o'};
fix_s = {'D_i', 'P', 'off'};
%%
P_fix = sort([logspace(0, 3, 15), 5, 150]);
fix_params = {0.01, P_fix, 1000};
fix_params = combvec(fix_params{:});
prec = [40*ones(1, 9), 60*ones(1, 8)];
fit_seed = 0.25*P_fix;
fit_seed =  repmat(fit_seed, 1, length(T));
skip = 1:4;
tic
[fval, fit_seed] = run_fits(T, fix_s, fix_params, fit_s, fit_seed,...
                            skip, 'droplet', prec);
toc
save([pa, 'Partitioning_vs_Dout/sims_P30_prec60.mat']);
%% Get parameter set for each minimum from above, check agreement with input
% load([pa, 'Partitioning_vs_Dout/sims_P18_prec20.mat']);
% te_seed = fit_seed;
% te_fval = fval;
% load([pa, 'Partitioning_vs_Dout/sims_P18.mat']);
% fval(1:length(P_fix)) = te_fval(1:length(P_fix));
% fit_seed(1:length(P_fix)) = te_seed(1:length(P_fix));
[~, ind] = nanmin(reshape(fval, length(P_fix), length(T)));
fit_seed_r = reshape(fit_seed, length(P_fix), length(T));
fval_r = reshape(fval, length(P_fix), length(T));
for i = 1:length(T)
    disp(fit_seed_r(ind(i), i));
end
%% Problem with high partitioning are numerical, higher accuracy leads to power law.
figure
loglog(fix_params(2, :), fit_seed_r(:, :));
% plot(P_fix, P_fix*1)
figure;
loglog(fix_params(2, :), fval_r, 'LineWidth', 2)
shg;
%%
csvwrite('Part_vs_Do_220121.csv', [fix_params(2, :); fit_seed_r']);
csvwrite('Part_vs_Cost_220121.csv', [fix_params(2, :); fval_r']);

%%
i = 1;
to_min(fit_seed(90)/30, fit_s, fix_params(:, 30), fix_s, T{3}, 'droplet')

%% Valley in parameter space
P_fix = logspace(0, 3, 100);
D_o = logspace(-2, 1, 100);
fix_s = {'D_i', 'P', 'off', 'D_o'};
fix_params = {0.01, P_fix, 1000, D_o};
fix_params = combvec(fix_params{:});
prec = 5*ones(size(fix_params, 2), 1);
[fval, fit_seed] = run_fits(T(2), fix_s, fix_params, {},...
                            nan(1, size(fix_params, 2)), 1, 'droplet', prec);
save([pa, 'Partitioning_vs_Dout/Valley_220121.mat']);
%%
load([pa, 'Partitioning_vs_Dout/Valley_220121.mat']);
csvwrite([pa, 'Partitioning_vs_Dout/Valley_220121.csv'],...
         [P_fix', D_o', reshape(fval, 100, 100)']);
%% Fine valley in parameter space
P_fine = logspace(1.9, 2.5, 1500);
D_o_fine = logspace(-1.5, 1, 1500);
% % P_fine = logspace(2.17, 2.19, 900);
% % D_o_fine = logspace(-1.02, -0.96, 900);
% P_fine = logspace(2.17, 2.185, 20);
% D_o_fine = logspace(-1.005, -0.99, 20);
fix_s = {'D_i', 'P', 'off', 'D_o'};
fix_params = {0.01, P_fine, 1000, D_o_fine};
fix_params = combvec(fix_params{:});
upper = fix_params(4, :) < 1.1*(fix_params(2, :)/1000).^1.2;
lower = fix_params(4, :) > 0.9*(fix_params(2, :)/1000).^1.2;
fix_params = fix_params(:, lower&upper);
prec = 5*ones(size(fix_params, 2), 1);
%%
[fval, fit_seed] = run_fits(T(2), fix_s, fix_params, {},...
                            nan(1, size(fix_params, 2)), 1, 'droplet', prec);
save([pa, 'Partitioning_vs_Dout/Valley_fine_220121.mat']);
%%
load([pa, 'Partitioning_vs_Dout/Valley_fine_220121.mat']);
dlmwrite([pa, 'Partitioning_vs_Dout/Valley_fine_210121.csv'],...
         [fix_params(2, :)', fix_params(4, :)', fval']);
