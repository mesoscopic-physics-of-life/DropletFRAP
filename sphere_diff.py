# Run this file in fenics environment (e.g. source activate fenicsproject)

import dolfin as df
import mshr as ms
import numpy as np
import time
# import logging
# logging.getLogger('FFC').setLevel(logging.WARNING)
df.set_log_level(40)
domain = ms.Sphere(df.Point(0, 0, 0), 1.0)
mesh = ms.generate_mesh(domain, 20)
# mesh = df.UnitSquareMesh(20,20)

dt = 0.02
diff = 0.1

F = df.FunctionSpace(mesh, 'CG', 1)

c0 = df.Function(F)
c = df.Function(F)
tc = df.TestFunction(F)

# # Initial conditions with gaussian distribution within droplet
# exp = '10*exp(-(x[0]*x[0]+x[1]*x[1]+x[2]*x[2])/(0.4*0.4))'
# c0.interpolate(df.Expression(exo, degree=1))

# # Uniform initial concentration
# c0.interpolate(df.Expression('100', degree=1))

# Half sphere bleached (check p.88/Using expressions to define subdomains
# in Fenics manual):
c0.interpolate(df.Expression('x[1] <= 0.0 + tol ? zero : full', degree=0,
                             tol=1E-14, zero=0, full=100))

# Define class for initial conditions for more flexibility
class InitCond(df.UserExpression):
    def eval(self, value, x):
        if x[0] <= 0:
            value[0] = 0
        else:
            value[0] = 100
    def value_shape(self):
        return ()
    
f0 = InitCond()
c0.interpolate(f0)

# With flux imposed on boundary:
# Constant flux
# g = df.Expression('4')
# Flux dependent on concentration difference between in and out at boundary
form = ((df.inner((c-c0)/dt, tc) + diff * df.inner(df.grad(c), df.grad(tc))) *
        df.dx - diff *2* df.inner(100-c, tc)*df.ds)

# No flux on boundary
# form = (df.inner((c-c0)/dt, tc) + diff * df.inner(df.grad(c), df.grad(tc))) *
#         df.dx


t = 0
cFile = df.XDMFFile('conc.xmdf')
cFile.write(c0, t)

ti = time.time()

at0 = np.empty([100])
for i in range(100):
    df.solve(form == 0, c)
    at0[i] = (c0(0, 0.3, 0))
    df.assign(c0, c)
    t += dt
    cFile.write(c0, t)
print(time.time() - ti)

cFile.close()