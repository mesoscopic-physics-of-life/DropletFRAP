function [cost, T] = to_min(x, fit_s, x_fix, fix_s, mov, fit_domain, prec)

[D_i, D_o, P, off] = extract_params(fix_s, x_fix, fit_s, x);

% Check whether mov is Simulation or Experiment, assign t, a, D_i
wh = whos('mov');
if strcmp(wh.class, 'Ternary_model')
    t_sim = mov.t;
    a = -1;
    off = 0.08*exp(-abs(off));
elseif strcmp(wh.class, 'FRAP_mov')
    si = size(mov.i_exp);
    D_i = D_i/(mov.pixel_size*si(2))^2;
    if ~strcmp(mov.edge_correction, 'Sim')
        t_sim = [0; mov.t+mov.time_step*(mov.post_frame)];
        a = -mov.edge_ind/mov.rad; % mov.edge corrected for PSF
        off = 0.08*exp(-abs(off))+mov.bleach_remainder/...
                mean(mov.pre_profile(1:mov.rad));
    else
        t_sim = [0; mov.t+mov.time_step];
        a = -mov.edge_ind/(mov.pixel_size*si(2));
        off = 0;
    end
end
b = 2e-05;
u0 = 0.5;
[e, e_g, u_g, u0] = calc_tanh_params(P, D_i, D_o, a, b, 0.5, 'asymmetric');
% simulate
T = Ternary_model(2, 'FRAP', {a, b, u0, e, e_g, u_g, 100, 0, 0, 0,...
                              'Constituent', off}, t_sim, prec);
lastwarn('') % Clear last warning message
T.solve_tern_frap();

% catch warning to check if simulation converged, if not, assign high cost
[warnMsg, ~] = lastwarn;
if ~isempty(warnMsg)
    cost = 10000000;
else
    if strcmp(wh.class, 'Ternary_model')
        if strcmp(fit_domain, 'droplet')
            x_i = linspace(0, -a-2*b, 100);
        elseif strcmp(fit_domain, 'all') % Use entire movie for fitting
            x_i = linspace(0, max(T.x), 1000);
        end
        sol = T.sol(2:end, :)';
        sol_interp = interp1(T.x, sol, x_i)/T.phi_t(1);
        i_exp = interp1(mov.x, mov.sol(2:end, :)'/mov.phi_t(1), x_i)';
    elseif strcmp(wh.class, 'FRAP_mov')
        ind = find(T.x>0.999, 1);
        sol = T.sol(2:end, 1:ind)/T.phi_t(1);
        i_exp = mov.i_exp;
        x_i = length(sol(1, :))/length(i_exp(1, :))*...
                            (1:length(i_exp(1, :)))*0.999;
        sol_interp = interp1(T.x(1:ind)*length(sol(1, :)), sol',x_i);
    end
    
    % Calculate cost function. More time points lie towards
    % phi_tot_in than toward bleach_remainder. This needs to be corrected
    % for, because the dynamics should be well fit throughout the entire
    % time course, not only towards the end. Therefore, histcounts counts
    % how many time points have av. int. inside within a certain percentage
    % interval. This is used as weighting factor N(B).
    [N, ~, B] = histcounts(mean(i_exp, 2), 20);
    cost = sum(sum((sol_interp'-i_exp).^2./mean(i_exp, 2)./N(B)'))/numel(i_exp);
%     figure(21); hold on;
%     plot(x_i, sol_interp(:, 1:1:end), 'b');
%     plot(x_i, i_exp(1:1:end, :), 'r')

%         cost = sum(sum(((T.sol-mov.sol)./mov.phi_t).^2));
end
    % Alternatively: fit total intensity:
    % cost = sum((sum(sol_interp)-sum(i_exp')).^2);

%     T.plot_sim('plot', 2, 'green', 2);
%     mov.plot_sim('plot', 2, 'r', 2);
%     l_rat = length(sol(1, :))/length(i_exp(1, :));
%     x_temp = (0:length(i_exp(1, :))-1)*0.999;
%     plot(l_rat*x_temp/max(l_rat * x_temp),...
%          i_exp(1:2:end, :)'*T.phi_t(1), 'r')
disp(['Cost is: ', num2str(cost), '.']);
end