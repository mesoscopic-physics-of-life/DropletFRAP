function t_centered = center_circles(t, rad)
% name = 'Lars_PGL6_to_register.tif';
% name = 'test.tif';
% info1 = imfinfo(name);
% width = max([info1.Width]);
% height = max([info1.Height]);
% t = zeros(height, width, length(info1));
si = size(t);
centers = zeros(si(3), 2);
for k = 1 : si(3)
%     t(:, :, k) = imread(name, k, 'Info', info1); 
    filt = imgaussfilt(t(:, :, k), 6);
    [c, radii] = imfindcircles(filt, [rad-10, rad+10], 'Sensitivity', 0.6);
    [~, ind] = max(radii);
    centers(k, :) = c(ind, :);
    disp(radii);
end 
%% Shift original frames to register image and only keep centered subregion
% rad = 60;
centers = round(centers);
t_centered = zeros(4*rad+1, 4*rad+1, si(3));
for ii = 1:si(3)
    t_centered(:, :, ii) = t(centers(ii, 2)-2*rad:centers(ii, 2)+2*rad,...
                             centers(ii, 1)-2*rad:centers(ii, 1)+2*rad, ii);
end
% %%
% for i = 1:si(3)
%     figure(1);
%     imshow(t_centered(:,:,i), []);
% %     figure(2);
% %     imshow(t(:,:,i), []); hold on;
% %     plot(centers(i, 1), centers(i, 2), 'r.', 'MarkerSize', 10);
%     pause();
% end
% %%
% for i = 1:si(3)
%     imshow(t(:,:,i), []);
%     pause();
% end
% %%
% figure; hold on;
% plot(radialavg(mean(t, 3), si(2)));
% pause()
% plot(radialavg(mean(t_centered, 3), length(t_centered(:, 1, 1))));
% %%
% figure; hold on;
% plot(radialavg(t(:, :, 364), si(2)));
% pause()
% plot(radialavg(t_centered(:, :, 364), length(t_centered(:, 1, 1))));
% end