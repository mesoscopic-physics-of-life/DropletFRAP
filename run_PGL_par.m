%% PGL-3
addpath(genpath('/home/hubatsch/frap_theory'));
pa = '/data/biophys/hubatsch/MatlabWorkspaces/';
load([pa, '/DiffusionMeasurements/PGL.mat'], 'f')
f_temp = f;
fix_s = {'D_i', 'P'};
fit_s = { 'D_o', 'off'};
P = logspace(0, 4.4, 14);
prec = [5, 5, 5, 10, 10, 10, 15, 15, 20, 25, 25, 25, 30, 30]; 
fix_params = {nan, P};
fix_params = combvec(fix_params{:});
fit_seed = ones(2, length(fix_params));
fit_seed(1, :) = 5*(0.2-0.00005)/(1000-1).*P;
fit_seed = repmat(fit_seed, 1, length(f));
skip = 1:length(f_temp);
tic
[fval, fit_seed, n_f, fix_params] =  run_fits(f_temp, fix_s, fix_params,...
                                    fit_s, fit_seed, skip, 'droplet', prec);
toc
save([pa, 'Partitioning_vs_Dout/PGL_P_vs_D_cluster_200226.mat']);
%%
% [~, ind] = min(fval);
% ind = 12;
% mov = f_temp{n_f(ind)};
% D = mov.x_seed*mov.conv_factor_D;
% to_min(fit_seed(:, ind), fit_s, [D, fix_params(2, ind)], fix_s,...
%         f_temp{n_f(ind)}, 'droplet', 5);