function fmin = min_sub_prod(x, i_exp)
% NEED to reintroduce y as a paramter, to allow for reaction outside!!!,
% typically 0.499 for now

% chi_s = -abs(x(1));
% Di_s = abs(x(2));
% Do_s = abs(x(3));
% chi_p = -abs(x(4));
% Di_p = abs(x(5));
% Do_p = abs(x(6));
% scale = abs(x(7));
% r = abs(x(8));
% t0 = abs(x(9));


chi_s = x(1);
Di_s = x(2);
Do_s = 100;
chi_p = x(3);
Di_p = x(4);
Do_p = 100;
scale = x(5);
r = x(6);
t0 = x(7);

[e, e_g, u_g, ~] = calc_tanh_params(10, Di_s, Do_s, -1, 0.02, 0.9, 'asymmetric');
params = {-1, 0.02, 0.5, e, e_g, u_g, 100, 0, 0, -1,...
          'Hammer_Ribo', 0};
[e, e_g, u_g, ~] = calc_tanh_params(10, Di_p, Do_p, -1, 0.02, 0.9, 'asymmetric');
par_n = struct('a', -1, 'b', 0.02, 'u0', 0.5, 'e', e,...
                  'e_g0', e_g, 'u_g0', u_g, 'v', 0, 'chi_s', chi_s,...
                  'chi_p', chi_p, 'y', 0.499, 'r', r);
t = [0, t0 + linspace(0, 2, size(i_exp, 1))];
T = Ternary_model(0, 'Substrate', params, t, 0.1, par_n);
try
    T.solve_tern_frap();

    x_l = linspace(0, 0.95, size(i_exp, 2));

    % T.plot_sim('plot', 10, 'red', 2)
    model = T.sol(:, :, 1) + T.sol(:, :, 2);
    model = interp1(T.x, model', x_l)';
    try 
        fmin = sum((scale*model(2:end, :)-i_exp).^2, 'all');
        figure(1); cla; hold on;
        plot(x_l, i_exp(1:100:end, :), 'r')
        plot(x_l, scale*model(2:100:end,:), 'b')
%         csvwrite('exp.csv', [x_l', i_exp(1:30:end, :)']);
%         csvwrite('theory.csv', [x_l', scale*model(2:30:end,:)']);
        shg;
    catch
        fmin = 10^10;
    end
catch 
    fmin = 10^10;
end
disp(fmin);
disp(x);
end