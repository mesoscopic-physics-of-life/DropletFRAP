function make_graph_pretty(xlab, ylab, tit, lims)
    %Set Color map to standard python CN Category 10.
    Python_color=['#1f77b4';'#ff7f0e';'#2ca02c';'#d62728';'#9467bd';'#8c564b';'#e377c2'];
    Python_map = hex2rgb(Python_color);
    set(groot,'DefaultAxesColorOrder',Python_map);
    
    % Set font sizes
    axis(lims);
    ax = gca();
    ax.FontSize = 16;
    xlabel(xlab,'FontSize', 16);    
    ylabel(ylab,'FontSize', 16);
    title(tit,'Interpreter','latex','FontSize', 16);
    
    %Square Plot
    axis square;
    
    % no frame
    box off;
    
    % Change line thickness of all lines
    lines = findobj(gcf,'Type','Line');
    for i = 1:numel(lines)
      lines(i).LineWidth = 3.0;
    end
    
