function sol = substrate_cleavage(x, t, D, k, geometry, x_full,...
                           initial_profile, time_full, bc_data)
% SIMPLE_DROP Solve for simple diffusion within droplet numerically.
%   x ... spatial domain for solution
%   t ... time points to return
%   D ... diffusion coefficient inside droplet
%   h ... flux rate, if Neumann BCs
%   x_full ... coordinates at which data are given for interpolation of ICs
%   initial_profile ... if ICs are read off data this is 1st time point
%   time_full ... time points at which BCs are provided from data
%   bc_data ... data points at which BCs are provided
%   geometry ...  2: spherical, 1: cylindrical, 0: slab (see pdepde)

% Define anonymous function to be able to pass parameters
pde_simple = @(x, t, u, DuDx) substrate_cleave_pde(x, t, u, DuDx, D, k);
options = odeset('RelTol',1e-6,'AbsTol',1e-7);

% BC read-out by experiments. ICs depend on data.
%     bc_data = sort(sgolayfilt(bc_data, 3, 5));
ic = @(x) ic_sub_cleave(x, x_full, initial_profile);
bc = @(xl,ul,xr,ur,t) bc_Diri(xl,ul,xr,ur,t, D, time_full, bc_data);
sol = pdepe(geometry, pde_simple, ic, bc, x, t, options);

function [c,f, s] = substrate_cleave_pde(x, t, u, DuDx, D, k)
c = [1; 1];
f = [D(1);D(2)].*DuDx;
s = [-k; k].*u(1);
end

function u0 = ic_sub_cleave(x, x_full, initial_profile)
u0 = [interp1(x_full, initial_profile(1, :), x); ...
      interp1(x_full, initial_profile(2, :), x)];
end

function  [pl,ql,pr,qr] = bc_Diri(xl, ul, xr, ur, t, D, time_full,...
                                         bc_data)
% No flux left
pl = [0; 0];
ql = [1; 1];
% Use experimental data for Dirichlet boundary conditions.
qr = [0; 0];
% Spline works better, benchmark done with theoretical curves from ternary
pr = [interp1(time_full, bc_data(1, :), t, 'spline')-ur(1);...
      interp1(time_full, bc_data(2, :), t, 'spline')-ur(2)];
end

end