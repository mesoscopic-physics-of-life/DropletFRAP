function sol = simple_drop_analytical(x, t, D, h)
% Analytical solution to simple drop, radius r0 taken as 1.

r0 = 1;
lam = 1 - h*r0;
e = zeros(1, 12);
epsi = 0.001:0.001:pi/2;

for i = 1:length(e)
    e_t = intersections(epsi, epsi/lam, epsi, tan(epsi));
    if isempty(e_t)
        cost = 10000000;
        sol = -1;
        return;
    end
    e(i) = e_t(1);
    epsi = epsi + pi;
end

sol = zeros(length(t), length(x));

for i = 1:length(t)
    term = @(e, x, t) ...
            2*h*r0*sin(e*x./r0)./(e*x./r0)*exp(-D*e^2/r0^2*t)/(e*csc(e)-cos(e));
    v = term(e(1), x, t(i)) + term(e(2), x, t(i)) + term(e(3), x, t(i)) + ...
        term(e(4), x, t(i)) + term(e(5), x, t(i)) + term(e(6), x, t(i)) + ...
        term(e(7), x, t(i)) + term(e(8), x, t(i)) + term(e(9), x, t(i)) + ...
        term(e(10), x, t(i)) + term(e(11), x, t(i)) + term(e(12), x, t(i));
    sol(i, :) = 1-v;
end