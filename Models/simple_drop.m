function sol = simple_drop(x, t, D, h, geometry, x_full,...
                           initial_profile, time_full, bc_data)
% SIMPLE_DROP Solve for simple diffusion within droplet numerically.
%   x ... spatial domain for solution
%   t ... time points to return
%   D ... diffusion coefficient inside droplet
%   h ... flux rate, if Neumann BCs
%   x_full ... coordinates at which data are given for interpolation of ICs
%   initial_profile ... if ICs are read off data this is 1st time point
%   time_full ... time points at which BCs are provided from data
%   bc_data ... data points at which BCs are provided
%   geometry ...  2: spherical, 1: cylindrical, 0: slab (see pdepde)

% Define anonymous function to be able to pass parameters
pde_simple = @(x, t, u, DuDx) simple_drop_pde(x, t, u, DuDx, D);
options = odeset('RelTol',1e-6,'AbsTol',1e-7);

if nargin == 5
    % BC proportional to difference with SS, ICs = 0.
    bc = @(xl,ul,xr,ur,t) bc_Neu(xl,ul,xr,ur,t, D, h);
    sol = pdepe(geometry, pde_simple, @simple_drop_ic, bc, x, t, options);
elseif nargin == 7
    % BC proportional to difference with SS, ICs depend on data.
    bc = @(xl,ul,xr,ur,t) bc_Neu(xl,ul,xr,ur,t, D, h);
    ic = @(x) ic_time_delay(x, x_full, initial_profile);
    sol = pdepe(geometry, pde_simple, ic, bc, x, t, options);
elseif nargin == 9
    % BC read-out by experiments. ICs depend on data.
%     bc_data = sort(sgolayfilt(bc_data, 3, 5));
    ic = @(x) ic_time_delay(x, x_full, initial_profile);
    bc = @(xl,ul,xr,ur,t) bc_Diri(xl,ul,xr,ur,t, D, h, time_full, bc_data);
    sol = pdepe(geometry, pde_simple, ic, bc, x, t, options);
end 

function [c,f, s] = simple_drop_pde(x, t, u, DuDx, D)
c = 1;
f = D*DuDx;
% s = D*2/x*DuDx; % From spherical coordinates, only if m = 0!!!
s = 0;
end

function  u0 = simple_drop_ic(x)
u0 = 0;
end

function u0 = ic_time_delay(x, x_full, initial_profile)
u0 = interp1(x_full, initial_profile, x);
end

function  [pl,ql,pr,qr] = bc_Neu(xl, ul, xr, ur, t, D, h)
% No flux left
pl = 0;
ql = 1;
% Exponential dependence right
pr = h*(1-ur);
qr = -1/D;
end

function  [pl,ql,pr,qr] = bc_Diri(xl, ul, xr, ur, t, D, h, time_full,...
                                         time_profile)
% No flux left
pl = 0;
ql = 1;
% Use experimental data for Dirichlet boundary conditions.
qr = 0;
% Spline works better, benchmark done with theoretical curves from ternary
pr = interp1(time_full, time_profile, t, 'spline')-ur;
% ff = @(x) 0.4941*(1-exp(-x/5.604))+0.207835;
% pr = ff(t)-ur;
end

end