function sol = gel_drop(x, t, D, h, k_off, k_on)
%% Solve for fixed cluster with on/off + diffusion inside
% Define anonymous function to be able to pass parameters
gel_drop_an = @(x, t, u, DuDx) gel_drop(x, t, u, DuDx, D, k_off, k_on);
gel_drop_bc_an = @(xl, ul, xr, ur, t) gel_drop_bc(xl, ul, xr, ur, t, D, h);
% Solve PDE
sol = pdepe(0, gel_drop_an, @gel_drop_ic, gel_drop_bc_an, x, t);

%% Function definitions for systems above
function [c,f,s] = gel_drop(x, t, u, DuDx, D, k_off, k_on)
c = [1; 1];
f = [D*DuDx(1); 0];
F = -k_on*u(1) + k_off*u(2);
s = [D*2/x*DuDx(1) + F; -F]; % This comes from spherical coordinates
end

function  u0 = gel_drop_ic(x)
u0 = [0; 0];
end

function  [pl,ql,pr,qr] = gel_drop_bc(xl, ul, xr, ur, t, D, h)
pl = [0; 0];
ql = [1; 1];
pr = [h*(1-ur(1)); 0];
qr = [-1/D; 1];
end

end