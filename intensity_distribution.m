%% Load tiff stack
% name = 'FUS tw a few min_Frap-1.tif';
% name = 'PGL3_7mM_0p1s_Fraptw0-1.tif';
name = 'halfFrapBig_t000000_cropped.tif';
% name = 'halfFrapBig_t000000_cropped_black.tif';
info1 = imfinfo(name);
width = max([info1.Width]);
height = max([info1.Height]);
t = zeros(height, width, length(info1));
for k = 1 : length(info1)
    t(:, :, k) = imread(name, k, 'Info', info1); 
end 
% t = t(:, :, 40:end);
thresh = t;
si = size(t);
% thresh(thresh < 900) = 0; %900 for PGL, 2300 for FUS
%%
% hold on;
% plot(squeeze([sum(thresh, [1, 2])]));
for i = 50:length(info1)
surf(thresh(:, :, i));
axis([0, si(3), 0, si(2), min(thresh(:)), max(thresh(:))]);
f = gcf;
f.Position = [10 10 900 900];
view(50, 70);
pause(0.03);
end
%% Create matrix with elements being distance from center of circle
dist = zeros(si);
% s = movmean(t, 3, 3); % Moving mean of three time slices
intensity = zeros(1, si(3));
center_all = zeros(2, si(3));
radius = 25;
for zz = 1:si(3)
    [i, j] = find(thresh(:, :, zz)>0);
    center = [mean(j), mean(i)];
    center_all(:, zz) = center;
    for ii = 1:si(1)
        for jj = 1:si(2)
            dist(ii, jj, zz) = sqrt(sum((center-[jj, ii]).^2));
        end
    end
%     f = t(:, :, zz);
%     imshow(f.*(dist(:, :, zz)<radius), []); hold on;
%     intensity(zz) = sum(f.*(dist(:, :, zz)<radius), 'all');
%     plot(mean(j), mean(i), 'r.', 'MarkerSize', 10);
%     fig = gcf;
%     fig.Position = [10 10 600 900];
%     viscircles([mean(j), mean(i)],radius)
%     viscircles([mean(j)+5, mean(i)+3],8)
%     pause(0.1);
end
%% Shift original frames to register image and only keep centered subregion
rad = 27;
t_centered = zeros(2*rad+1, 2*rad+1, si(3));
for ii = 1:si(3)
    t_centered(:, :, ii) = t(center_all(2, ii)-rad:center_all(2, ii)+rad,...
                             center_all(1, ii)-rad:center_all(1, ii)+rad...
                             , ii);
end
%% Next fancy thing: fit 3D gaussian to profile after subtracting shape  of 
%  mean droplet

% 1.: mean profile of all droplets
t_mean = imgaussfilt(mean(t_centered(:, :, 1:40), 3), 2);
% t_mean = mean(t_centered(:, :, 1:40), 3);
si = size(t_mean);
inten = zeros(1, 150);
for ii = 50:1:150
di = t_mean - t_centered(:, :, ii);
% surf(t_mean);
[I, J] = ind2sub(si(1:2), 1:si(1)*si(2));
fi = fit([I', J'], di(:), 'a1*exp(-((x-b1)^2+(y-b2)^2)/c1^2)',...
         'StartPoint', [700, 50, 50, 20]);
% fi.a1
figure(1); clf
axis([0, si(3), 0, si(2), min(thresh(:)), max(thresh(:))]);
% s = surf(di); hold on;
p = plot(fi); hold on;
inten(ii) = fi.a1;
% p.EdgeColor = 'none';
% p.FaceColor = 'g';
% s.EdgeColor = 'none';
% pause(0.1)
end
plot(inten)

%% Get experimental intensity distribution of FRAP data + circles
imshow(t(:, :, 25), []); hold on; 
no_z = 39;
no_t = si(3)/no_z;
si = size(t);
pix_si = 0.136;
radius = zeros(1, no_z);
cents = zeros(no_z, 2);
for i = 1:no_z
[centers, radii] = imfindcircles(t(:, :, i),[10 80]); 
[radius(i), ind] = max(radii);
cents(i, :) = centers(ind, :);
end
radius = radius * pix_si;
cents = cents * pix_si;
z_dist = 0.5*(1:no_z);
%% Get z coordinate of centre by fitting r_new to radius
func = @(r_max, z_offset, x) sqrt(r_max^2 - (x-z_offset).^2);
ft = fittype(func);
fo = fitoptions('Method', 'NonlinearLeastSquares','StartPoint', [max(radius), 10]);
% Only use radii in the middle of sphere as those are most precise
fi = fit(z_dist(10:30)', radius(10:30)', ft, fo);
%%
figure; plot(z_dist, radius, 'LineWidth', 3); hold on;
plot(z_dist, fi(z_dist), '--', 'LineWidth', 3)
r_fit = sqrt((fi.r_max)^2-(z_dist-fi.z_offset).^2);
figure;
plot(radius); hold on; plot(r_fit)
[X, Y, Z] = meshgrid(pix_si * (1:si(1)), pix_si * (1:si(2)), 0.5 * (1:no_z));

%% Plot sliced data
tf = 11; % time frame    
figure;
for tf = 11:200
slice(X, Y, Z, t(:, :, (tf*no_z+1):(tf+1)*no_z), 13.6, 13.6, 7.5);
shading flat; hold on; axis equal;
caxis manual
caxis([500 2600]);
saveas(gcf, ['fig_', num2str(tf), '.png']);
end
%%
% r_fit = r_fit/max(r_fit);
% [X, Y] = meshgrid(-1:0.1:1, -1:0.1:1);
% R = sqrt(X.^2+Y.^2);
% x_all = []; y_all = []; z_all = [];
% 
% for i = 1:length(z_dist)
%     x_temp = X(R<r_fit(i));
%     y_temp = Y(R<r_fit(i));
%     x_all = [x_all; fi.r_max * x_temp(:) + mean(cents(:, 1))];
%     y_all = [y_all; fi.r_max * y_temp(:) + mean(cents(:, 2))];
%     z_all = [z_all; z_dist(i) * ones(length(y_temp(:)), 1)];
% end
% 
% %% Threshold radius to only use values inside droplet
% R = sqrt((x_all-mean(cents(:, 1))).^2 + (y_all-mean(cents(:, 2))).^2 + ...
%          (z_all-fi.z_offset).^2);
% r_thresh = 0.95*max(R(:));
% x_all = x_all(R<r_thresh);
% y_all = y_all(R<r_thresh);
% z_all = z_all(R<r_thresh);
%%
r_fit = r_fit/max(r_fit);
[X_grid, Y_grid, Z_grid] = meshgrid(pix_si*(1:si(1)), pix_si*(1:si(2)), 0.5 * (1:no_z));
R = sqrt((X_grid-cents(19, 1)).^2 + (Y_grid-cents(19, 2)).^2 + ...
         (Z_grid-fi.z_offset).^2);
X = X_grid(R<0.90*fi.r_max);
Y = Y_grid(R<0.90*fi.r_max);
Z = Z_grid(R<0.90*fi.r_max);
plot3(X, Y, Z, 'r.');axis equal;
%%
plot3(x_all, y_all, z_all, 'b.', 'MarkerSize', 1);
m = mean(cents, 1);
% plot3(mean(x_all), mean(y_all), mean(z_all), '.', 'MarkerSize', 1200)
% plot3(pix_si*m(1), pix_si*m(2), fi.z_offset*pix_si, '.', 'MarkerSize', 1200)
%% Export centre coordinates of sphere to python
dlmwrite('mid_point_and_radius.csv', ...
         [mean(cents(:, 1)), mean(cents(:, 2)), fi.z_offset, fi.r_max])
% dlmwrite('xyz_grid.csv', [x_all-mean(cents(:, 1)), y_all-mean(cents(:, 2)),...
%                           z_all-fi.z_offset]);
dlmwrite('xyz_grid.csv', [X-mean(cents(:, 1)), Y-mean(cents(:, 2)),...
                          Z-fi.z_offset]);
% plot3(x_all-mean(cents(:, 1)), y_all-mean(cents(:, 2)),...
%                           z_all-fi.z_offset)
                      
%% Read in interpolated values from python simulation to compare for fit
ints_simu = csvread('~/Desktop/FRAP/DropletFRAP/interp_intens_simu.csv');
for tp = 1:10:no_t
t_start = t(:, :, (tp*no_z+1):(tp+1)*no_z);
ints_exp = t_start(R<0.95*fi.r_max);
dlmwrite(['ints_exp_black_', num2str(tp), '.csv'], ints_exp);
end