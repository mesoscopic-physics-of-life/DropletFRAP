% [ti, cents, si, radius] = analyse_image('tseries_test.tif');
% [ti, cents, si, radius] = analyse_image('tseries1120210921_reg_a.tif');
% [ti, cents, si, radius] = analyse_image('tseries720210921_30432_reg_cro.tif');
[ti, cents, si, radius] = analyse_image('tseries620210921_reg_a.tif');
sz = size(ti);
%% with edge detection
ti_edge = zeros(size(ti));
for i = 1:sz(3)
    ti_edge(:, :, i) = edge(ti(:, :, i), 'sobel');
end
%%
ti_edge = imgaussfilt(double(ti_edge), 2);
row = 0.5:sz(1)-0.5;
col = 0.5:sz(2)-0.5;
t = (1:sz(3))';
F = griddedInterpolant({row,col,t}, double(ti_edge));
F_orig = griddedInterpolant({row,col,t}, double(ti));
step = 0.125;
row_q = (step/2:step:sz(1))';
col_q = (step/2:step:sz(2))';
vq = F({row_q, col_q, t});
vq_orig = F_orig({row_q, col_q, t});
%%
figure; hold on;
imshow(vq_orig(:, :, 1), []);
roi = drawcircle;
%%
roi_cen = roi.Center;
r = roi.Radius;
cost = zeros(11, 11);
cen_col = roi_cen(1);
cen_row = roi_cen(2);
cs = zeros(201, 2);
cs(1, :) = [cen_col, cen_row];
n = vq(round(cen_row-r):round(cen_row+r), round(cen_col-r):round(cen_col+r), 1);
%%
for im = 1:100
%     n = vq(cen_row-r:cen_row+r, cen_col-r:cen_col+r, im);
    for i = 1:11
        for j = 1:11
            shift_row = i-6;
            shift_col = j-6;
            n_1 = vq(cen_row-r+shift_row:cen_row+r+shift_row,...
                       cen_col-r+shift_col:cen_col+r+shift_col, im+1);
            cost(i, j) = sum((n-n_1).^2, 'all');
        end
    end
    [~,I] = min(cost,[],'all', 'linear');
    [row, col] = ind2sub(size(cost), I);
    cen_row = cen_row + row - 6;
    cen_col = cen_col + col - 6;
    cs(im+1, :) = [cen_col, cen_row];
    imshow(vq_orig(:, :, im+1), []);
    drawcircle('Center', [cen_col, cen_row], 'Radius', r);
    shg;
    pause()
end
%%
S = Segmentation({'/Users/hubatsch/Desktop/DataLars_putbackonserver/21_09_21/RPS-15_FRET_2/tseries1120210921_reg_a_to_del.tif'}, 6, 2)
%%
S = Segmentation({'/Users/hubatsch/Desktop/DataLars_putbackonserver/21_09_27/RPS-15_FAM/tseries1320210927_31513_reg.tif'}, 6, 2);
%% 
S = Segmentation({'/Users/hubatsch/Desktop/DataLars_putbackonserver/21_09_21/RPS-15_FAM/tseries620210921_reg_a.tif'}, 6, 2);
