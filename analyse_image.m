function [ti, si] = analyse_image(name)
% ANALYSE_IMAGE gets name of stack, outputs stack, center position of
% droplet, size of the stack and radius of the droplet

% Get  stack dimensions
info1 = imfinfo(name);
width = max([info1.Width]);
height = max([info1.Height]);

% Read images
ti = zeros(height, width, length(info1));
for k = 1 : length(info1)
    ti(:, :, k) = imread(name, k, 'Info', info1); 
end

si = size(ti);
end