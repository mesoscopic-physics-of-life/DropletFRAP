**DropletFRAP: extract diffusion coefficient inside a droplet.**

The entire repository should be on the Matlab path.

The extraction process requires a FRAP movie that contains a single droplet, centered in the field of view. Registration and cropping can be done Fiji. The field of view should include sufficient area of the bulk phase, in order to reliably extract background. Thus, a cross section in Fiji (e.g. using the line tool) should show a flat concentration profile in the bulk, away from the droplet.

The movie should be split in two parts, prebleach (suffix '_prebleach') and postbleach (suffix '_a'), e.g. 11637_prebleach.tif and 11637_a.tif.
The sample data set can be downloaded from [here](https://drive.google.com/file/d/1yT-IZb4AE7g1Bv83DNzE2tZegy-uVTDR/view?usp=sharing). This needs to be unzipped.

In Matlab run the following commands to obtain a diffusion coefficient.

```
cd 11637
m = FRAP_mov('11637', 0.136, 30, 1, '0.07', 250, 20, 'simple_drop', 1, 100);
m.calc_Sim;
```

With the following example parameters
    '11637' ... movie name
    0.136   ... pixel size (um)
    30      ... droplet radius in pixels, err on the smaller side.
    1       ... increment step size in the time course. Larger than one if frames should be skipped.
    '2'     ... frame interval (s)
    250     ... number of frames to be analysed
    20      ... starting frame (counted from testmovie_a.tif)
    'simple_drop' ... PDE model
    1       ... edge correction (normalize out intensity increase twoards middle of droplet)
    100     ... camera background

m will contain an instance of the class FRAP_mov, and the diffusion coefficient can be obtained using

`m.x_seed*m.conv_factor_D`

Using 

`m.plot_movie(8, 0, 1)`

will plot every 8th frame as a movie if you keep pressing any key.

