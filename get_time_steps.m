function [ts, time_step] = get_time_steps(fname, frap_frame, l_t, post_frame)
% GET_TIME_STEPS takes meta data file, get all lines containing a certain
% string and returns the time steps.
if ~ischar(frap_frame)
    fid = fopen([fname, '.csv']);
    tline = fgetl(fid);
    tlines = cell(0,1);
    while ischar(tline)
        tlines{end+1,1} = tline;
        tline = fgetl(fid);
    end
    fclose(fid);
    formatIn = 'dd/mm/yyyy HH:MM:SS.FFF';
    tlines = tlines(cellfun(@(x) contains(x, 'Timestamp for '), tlines));
    tlines = cellfun(@(x) split(x, ','), tlines, 'Uni', 0);
    ts = cellfun(@(x) datenum(x{4}, formatIn), tlines);
    ts = diff(sort(ts))*24*60*60;
    try 
        time_step = mean(ts(frap_frame + post_frame:frap_frame +...
                        post_frame + l_t - 1));
        ts = cumsum([0; ts(frap_frame + post_frame:frap_frame + post_frame +...
                l_t - 1)]);
    catch
        time_step = mean(ts(frap_frame + post_frame:frap_frame +...
                        post_frame + 100));
        ts = (0:time_step:l_t*time_step)';
        disp(['Time step was calculated from only a few steps, check,',...
              'movie length!']);
    end
elseif contains(frap_frame, 'excel')
    ts = readtable([fname, '.xlsx']);
    frap_fr = str2double(frap_frame(1:end-5));
    ts = ts(frap_fr+post_frame:frap_fr+post_frame+l_t, 1).Variables;
    ts = ts - ts(1);
    time_step = nan;
else
    time_step = str2double(frap_frame);
    ts = (0:time_step:l_t*time_step)';
end