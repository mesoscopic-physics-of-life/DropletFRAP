function [D_i, D_o, P, off] = extract_params(fix_s, x_fix, fit_s, x)
% extract parameters that are fixed or free for fitting respectively

% Check which params are fixed
if any(strcmp(fix_s, 'D_i')); D_i = x_fix(strcmp(fix_s, 'D_i')); end
if any(strcmp(fix_s, 'D_o')); D_o = x_fix(strcmp(fix_s, 'D_o')); end
if any(strcmp(fix_s, 'P')); P = x_fix(strcmp(fix_s, 'P')); end
if any(strcmp(fix_s, 'off')); off = x_fix(strcmp(fix_s, 'off')); end
% Check which params are to be fitted
if any(strcmp(fit_s, 'D_i')); D_i = x(strcmp(fit_s, 'D_i')); end
if any(strcmp(fit_s, 'D_o')); D_o = x(strcmp(fit_s, 'D_o')); end
if any(strcmp(fit_s, 'P')); P = x(strcmp(fit_s, 'P')); end
if any(strcmp(fit_s, 'off')); off = x(strcmp(fit_s, 'off')); end

end