function [step, time_st_s, ind_s] = get_timestep(file)
% GET_TIMESTEP takes metadata file and returns average time step. This
% literally takes the average, so if there are any spikes in the time
% stepping, e.g. due to frap taking a long time this is not taken into
% account.

fid = fopen(file);
tline = fgetl(fid);
i = 1; % bit dirty, but easiest to save overhead on checking no of lines
while ischar(tline)
    time_st(i) = datenum(tline(end-11:end));
    start = strfind(tline, 'T=')+2;
    en = length(tline)-24;
    ind(i) = str2double(tline(start:en));
    tline = fgetl(fid); 
    i = i+1;
end
fclose(fid);
[ind_s, i] = sort(ind);
time_st_s = time_st(i);
step = mean(diff(time_st_s)*24*3600);