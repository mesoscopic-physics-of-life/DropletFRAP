x = 50:0.1:70;
diff_sol = 1/sqrt(4*pi*1*1)*exp(-(x-60).^2/(4*1*1));
plot(x, diff_sol);
%%
f_temp = f1(1);
fix_s = {'D_i', 'P', 'off'};
fit_s = { 'D_o'};
P = 5;
prec = 5; 
fix_params = {nan, P, 1000};
fix_params = combvec(fix_params{:});
fit_seed = 1;
skip = 1;
[fval, fit_seed, n_f, fix_params] =  run_fits(f_temp, fix_s, fix_params,...
                                    fit_s, fit_seed, skip, 'droplet', prec);
%%
T = Ternary_model(2, 'Gauss', {a, b, u0, e, e_g, u_g, 100, 60, 0,...
                              'Constituent', off}, t_sim, prec);
T.t = [0, 0.5, 1];
T.solve_tern_frap();
%%
T.plot_sim('plot', 1, 'green', 2); xlim([50, 100]);