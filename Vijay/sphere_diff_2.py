import dolfin as df
# from IPython.core.debugger import Tracer
import mshr as ms
import time
# import logging
# logging.getLogger('FFC').setLevel(logging.WARNING)
df.set_log_level(40)
domain = ms.Sphere(df.Point(0, 0, 0), 1.0)
mesh = ms.generate_mesh(domain, 15)
# mesh = df.UnitSquareMesh(20,20)

dt = 0.01
diff = 0.1

F = df.FunctionSpace(mesh, 'CG', 1)

c0 = df.Function(F)
c = df.Function(F)
tc = df.TestFunction(F)

# Inner gaussian with Vijay
c0.interpolate(df.Expression('10*exp(-(x[0]*x[0]+x[1]*x[1]+x[2]*x[2])/(0.4*0.4))', degree=1))
# Half sphere
# c0.interpolate(df.UserExpression('10*exp(-(x[0]*x[0]+x[1]*x[1]+x[2]*x[2])/(0.4*0.4))', degree=1))
# Tracer()()
# class InitialCondition(df.Expression):
#     def eval_cell(self, value, x, ufc_cell):
#         if x[0] <= 0.0:
#             value[0] = 1.0
#         else:
#             value[0] = 0.0
# c0.interpolate(InitialCondition())

# Tracer()()

form = (df.inner((c-c0)/dt, tc) + diff * df.inner(df.grad(c), df.grad(tc))) * df.dx


t = 0
cFile = df.XDMFFile('conc.xmdf')
cFile.write(c0, t)

ti = time.time()
for i in range(100):
    df.solve(form == 0, c)
    df.assign(c0, c)
    t += dt
    cFile.write(c0, t)
print(time.time() - ti)

cFile.close()

