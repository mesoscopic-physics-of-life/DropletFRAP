function FRAP_app(folder, name)
% folder ... folder where name can be found, e.g. /Users/hubatsch/Desktop/
% name ... name of dataset e.g. '11637'
cd([folder, name]);
m = FRAP_mov(name, 0.136, 30, 1, 20, 250, 20, 'simple_drop', 1, 100);
disp(m.x_seed*m.conv_factor_D);
