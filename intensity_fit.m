function cost = intensity_fit(r_n, t, x_fit, model, reference)
% INTENSITY_FIT uses integrated intensity data to fit model. This does
% explicitly NOT take into account space, to show that space is necessary
% to assign physical processes to different time scales
% reference - the fitting data
% r_fit_exp - not strictly speaking necessary, as we only use to_minimize()
%             to calculate v_fit for a given parameter set x, not comparing
%             to the actual data to evaluate a cost function

[~, v_fit] = to_minimize(r_n, t, x_fit, model, 1);
si_v = size(v_fit);
cost = sum((sum(v_fit/si_v(2), 2) - reference).^2);

