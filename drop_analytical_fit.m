%% Spherical droplet with Dirichlet BC, accord. to Fourier Ana. Theo. Heat
a = 1;
D = 0.1;
err = @(n, r, t) erfc(((2*n+1)*a-r)./(2*sqrt(D*t))) - ...
                 erfc(((2*n+1)*a+r)./(2*sqrt(D*t)));
c = @(r, t) a./r.*(err(0, r, t) + err(1, r, t) + err(2, r, t) +...
            err(3, r, t) + err(4, r, t) + err(5, r, t) + err(6, r, t) +...
            err(7, r, t) + err(8, r, t));
r = 0:0.01:1;
figure(1); hold on;
for i = 0.0:0.1:1
    inten = c(r,i);
    inten = inten(2:end);
    plot(r(2:end), inten);
end

%% Load tiff stack, get circles, calculate mean position of center
% % PGL-3, pixel size: 0.136, timestep = 1.16 s
% time_step = 1.16; pixel_size = 0.136;
% [ti, cents, si, radius] = analyse_image('fullfrap150mM 3D_t000000_cropped_zPlan10.tif');
% pre_bleach = ti(:, :, 10);
% ti = ti(:, :, 11:1:si(3));
% % x_seed = [0.3549    1.7962]; model = 'simple_drop';
% % x_seed = [0.4145    7.0627   19.5802   61.5760]; model = 'gel_drop'; 
% % x_seed = [0.0087   62.8990    2.3471    0.4940    0.7407]; model = 'two_fluid_drop';
% rad = 25;
% from_timepoint = 1;

% % PGL-3, April 2019, different ages
% % tw: 15 min
% file = 'Original Metadata - PGL3_150mM_fullfrap_tw15min_t000001.csv';
% time_step = 100*get_timestep(file);
% pixel_size = 0.136;
% [ti, ~, si, ~] = analyse_image('PGL3_150mM_fullfrap_tw15min_every10th.tif');
% [pre_bleach, ~, ~, ~] = analyse_image('10frames_prebleach.tif');
% pre_bleach = mean(pre_bleach(:, :, :), 3);
% ti = ti(:, :, 1:10:si(3));
% rad = 53;
% x_seed = [0.6929    1.6110]; model = 'simple_drop'; % START FROM 100!!!
% x_seed = [ 2.4624    3.1986    4.2289    5.7118]; model = 'gel_drop'; 
% % TWO FLUID DROP SHOULDN'T START FROM 100!!!
% x_seed = [0.9423    1.2308    0.0616  137.7122    0.7928]; model = 'two_fluid_drop';
% % This works ever so slightly better
% x_seed = [0.9425    1.2305    0.0761  111.7140    0.7925]; model = 'two_fluid_drop';

% %tw: 53 min
% file = 'Original Metadata - PGL3_150mM_fullfrap_tw53min20190410_122840 PM_t000000.csv';
% time_step = 10*get_timestep(file); % Only every 10th frame used
% pixel_size = 0.136;
% [ti, ~, si, ~] = analyse_image('PGL3_150mM_fullfrap_tw53min_every10th.tif');
% [pre_bleach, ~, ~, ~] = analyse_image('10frames_prebleach.tif');
% pre_bleach = mean(pre_bleach(:, :, :), 3);
% ti = ti(:, :, 1:10:si(3));
% rad = 61;
% x_seed = [0.8541    0.8622]; model = 'simple_drop'; % START FROM 150!!
% % x_seed = [ 0.4961    1.7482    2.0978    0.0008]; model = 'gel_drop';
% % x_seed = [1.0534    0.7532    0.0276  285.1638    0.8019]; model = 'two_fluid_drop';

% Coacervates
% tw: ~10 mins
% pixel_size = 0.136;
% [ti, ~, si, ~] = analyse_image('FRAP_ Coacervates_sample3120191205_24136 PM-2.tif');
% [pre_bleach, ~, ~, ~] = analyse_image(['FRAP_ Coacervates_sample3120191205_24136',...
%                           'PM_prebleach.tif']);
% pre_bleach = mean(pre_bleach(:, :, :), 3);
% % ti = ti(:, :, 1:si(3));
% skip_every_nth = 1;
% ti = ti(:, :, 1:skip_every_nth:end);
% time_step = 0.05*skip_every_nth;
% rad = 22;
% x_seed = [0.1424    1.5261]; model = 'simple_drop';
% Lars data
% tw: ~10 mins
% time_step = 5;
% pixel_size = 0.136;
% % [ti, ~, si, ~] = analyse_image('Lars_PGL6_cut.tif');
% [ti, ~, si, ~] = analyse_image('Lars_PGL6_c.tif');
% ti = center_circles(ti, 30);
% [pre_bleach, ~, ~, ~] = analyse_image('Lars_PGL6_prebleach.tif');
% pre_bleach = center_circles(pre_bleach, 30);
% pre_bleach = mean(pre_bleach(:, :, :), 3);
% ti = ti(:, :, 2:si(3));
% rad = 26;
% x_seed = [0.6929    1.6110]; model = 'simple_drop';

% tw: ~10 mins
time_step = 5;
pixel_size = 0.136;
% [ti, ~, si, ~] = analyse_image('Lars_PGL6_cut.tif');
[ti, ~, si, ~] = analyse_image('Lars_PGL520191213_112943_2to1_c.tif');
ti = center_circles(ti, 45);
[pre_bleach, ~, ~, ~] = analyse_image('Lars_PGL520191213_112943_2to1_prebleach.tif');
pre_bleach = center_circles(pre_bleach, 45);
pre_bleach = mean(pre_bleach(:, :, :), 3);
ti = ti(:, :, 2:si(3));
rad = 45;
x_seed = [0.6929    1.6110]; model = 'simple_drop';
% % FUS
% pixel_size = 0.136;
% file = 'FUSWT-GFP_200mM_tw1hr30_FRAP_fullandspot_1mintres.csv';
% time_step = get_timestep(file);
% fn = 'FUSWT-GFP_200mM_tw1hr30_FRAP_fullandspot_1mintres_onlyRec.tif';
% [ti, cents, si, radius] = analyse_image(fn);
% [pre_bleach, ~, ~, ~] = analyse_image('10_prebleach.tif');
% pre_bleach = mean(pre_bleach(:, :, :), 3);
% rad = 21;
% % no time delay: x_seed = [0.5198    0.3731], otherwise below:
% x_seed = [0.5338    0.3311    0.0660]; model = 'simple_drop'; % Works ok, not for start
% % x_seed = [1.5692 0.8367 6.7692   11.7085    0.0762]; model = 'gel_drop'; % works ok
% % x_seed = [0.5573 0.3045 0.0183 69.7708 0.9532 0.1010]; model = 'two_fluid_drop';

% % FUS, pixel size: 0.136, time interval: 100 ms
% time_step = 0.1*100; pixel_size = 0.136;
% [ti, cents, si, radius] = analyse_image('2D_fullandcircle_2019Feb18_crop.tif');
% pre_bleach = mean(ti(:, :, 1:10), 3);
% ti = ti(:, :, 11:100:si(3));
% rad = 16;
% % x_seed = [0.8231    6.4154    3.0106    4.2146]; model = 'gel_drop';
% % x_seed = [0.1875    5.8928]; model = 'simple_drop'; % Doesn't work!
% x_seed = [0.1761 1.4074 0.0467 54.5621 0.6363]; model = 'two_fluid_drop';

% % FUS 2, pixelsize=0.136 , time interval: 1.78 s
% time_step = 1.78; pixel_size = 0.136;
% [ti, cents, si, radius] = analyse_image('Combined.tif');
% pre_bleach = ti(:, :, 10);
% ti = ti(:, :, 11:si(3));
% rad = 21;
% from_timepoint = 1;
% x_seed = [0.5870 2.7016 0.3662 0.4054]; model = 'gel_drop';
% % x_seed = [0.0496    7.0389]; model = 'simple_drop';
% % x_seed = [0.0497 30.5186 0.1514 0.8041 0.3833]; model = 'two_fluid_drop';


%% Panel showing difference between radial average and simple 5 pixel line
figure; hold on;
plot(i_exp_no_avg(10, :)', 'LineWidth', 4, 'Color', [100/255, 100/255, 100/255]);
plot(i_exp(10, :)', 'LineWidth', 4, 'Color', [255/255, 217/255, 128/255]);
make_fig_pretty('x(pxls)', 'intensity (a.u.)');
axis([0, 35, 0, 0.12]);
pa = '/Users/hubatsch/ownCloud/Dropbox_Lars_Christoph/DropletFRAP/';
% print([pa, 'FRAP_paper/fig1_rad_vs_line'], '-depsc');

%% Sample plot for presentation
%% Same outcome, different rates: (tw53, PGL-3)
% x_seed = [1.6, 0.7];
% % x_seed = [0.6143    1.4644];
% x_seed = [2*12, 6*0.1];
x_seed = [2*0.02    4*10];
%% gel drop
% x_seed = [ 0.4961    1.7482    1   1]; model = 'gel_drop';
x_seed = [ 3.5    5    5   5]; model = 'gel_drop';
%% two fluid drop
x_seed = [1.0526 0.7535 0.0276 286.2482 0.8020]; model = 'two_fluid_drop';
% x_seed = [2    1    0.02  200.0    0.5]; model = 'two_fluid_drop';
%% Plot fit + data, print to file if needed for movie.
% [cost, v_fit, r_n, v_fit_end, v_fit_1, v_fit_2] = to_minimize(r_fit_exp,...
%                                                 x_seed, model, norm_fac);
i = 2;
[cost, v_fit, r_n, v_fit_end] = to_minimize(f{i}.i_exp, f{i}.t, f{i}.x_seed,...
    f{i}.model, f{i}.bc_exp);
si_v = size(v_fit);
%%
x = pixel_size*(0:si_v(2)-1);
figure(2);
for i = 1:si_v(1)
    clf; hold on; 
    plot(x, r_n(i, :), 'LineWidth', 3);
    plot(x, v_fit(i, :), '--', 'LineWidth', 3);%
%     plot(x, v_fit_end(:)/v_fit(end, end), '--', 'LineWidth', 3);
%     plot(x, v_fit_1(i, :), '--', 'Color', [0.9 0.7 0.5], 'LineWidth', 2);
%     plot(x, v_fit_2(i, :), '--','Color', [0.7 .7 0.7], 'LineWidth', 2);
    make_fig_pretty('r (\mum)', 'intensity (a.u.)');
%     axis([0, max(x), 0, max(v_fit_end)]); shg;
    axis([0, max(x), 0, 1.1]); shg;
%     print([num2str(i),'.png'],'-dpng')
    pause();
end
%% 
figure; hold on;
plot(x, r_n(1:4:200, :)', 'Color', [0.12156   0.4666666   0.7058823], 'LineWidth', 1.5);
% plot(v_fit(1:2:100, :)', '--', 'Color', [1.   0.49803   0.0549], 'LineWidth', 1.5);
make_graph_pretty('$x [\mu m]$', 'intensity [a.u.]', '');
axis([0, max(x), 0, max(r_n(:))]); shg;
% print('Timecourse_exp.png','-dpng')
%% Boundary for Frank
i = 4;
f1 = fit(f{i}.t, f{i}.bc_exp-min(f{i}.bc_exp), 'B-B*exp(-x/tau)', 'StartPoint',[2, 0.5]);
figure; hold on;
plot(f{i}.t, f{i}.i_exp(:, end)-min( f{i}.i_exp(:, end)), 'LineWidth', 1)
plot(f{i}.t, f{i}.bc_exp-min(f{i}.bc_exp), 'LineWidth', 1)
plot(f{i}.t, f1(f{i}.t))
make_graph_pretty('time [s]', 'intensity [a.u.]', '')
shg
%% Boundary difference between minimum and maximum
figure; hold on;
in = max(i_exp,[],2)-min(i_exp,[],2);
f1 = fit((101:201)', in(101:201), 'A-B*exp(-x/tau)', 'StartPoint',[2, 2, 100]);
plot(1:201, f1(1:201));
plot(max(i_exp,[],2)-min(i_exp,[],2));
%% Ratio between max and minimum
figure; hold on;
plot((max(i_exp,[],2)-100)./(min(i_exp,[],2)-100));
%% total intensity, only the part explained by diffusion
figure; hold on;
plot(sum(r_n, 2)./v_fit(:, end), 'LineWidth', 1)
plot(sum(v_fit, 2)./v_fit(:, end), 'LineWidth', 1)
%% Figures 
fig_fol = '~/ownCloud/Dropbox_Lars_Christoph/DropletFRAP/Latex/Figures/';
red = [0.8500, 0.3250, 0.0980];
blue = [0, 0.4470, 0.7410];

figure; hold on;
plot(sum(r_n')/si_v(2), 'LineWidth', 3);  
plot(sum(v_fit'/si_v(2)), 'LineWidth', 3);
plot(sum(v_fit_1'/si_v(2)), 'LineWidth', 3);
plot(sum(v_fit_2'/si_v(2)), 'LineWidth', 3);
make_fig_pretty('time (s)', 'intensity (a.u.)');
%% Trying to find degenerate D and h values that give rise to same I_tot
% for PGL-3 tw53 this works: 
% x_seed = [0.0378   19.8435] or x_seed = [0.5302    1.6535]
% for PGL-3 tw15 this works:
% x_seed = [0.5309    2.4562] or x_seed = [0.0444   25.2470]
experimental = sum(r_n')/si_v(2);
f_min = @(x) intensity_fit(i_exp, x, model, norm_fac, experimental);
opt = optimset('MaxFunEvals', 2000, 'PlotFcns',@optimplotfval);
for i = 1:6
    x_seed = fminsearch(f_min, x_seed, opt);
end
%% Cliff reproduction
model = 'simple_drop';
norm_fac = 2.27; % To get final measured intensity of 0.44
f_min = @(x) intensity_fit(repmat(y, 1, 10), x, model, norm_fac, y');
opt = optimset('MaxFunEvals', 2000, 'PlotFcns',@optimplotfval);
% x_seed = [60.4040    0.0748];
x_seed = [0.000005, 40000];
for i = 1:6
    x_seed = fminsearch(f_min, x_seed, opt);
end
%%
[cost, v_fit, ~, ~] = to_minimize(repmat(y, 1, 10), x_seed, model, norm_fac);
si = size(v_fit);
figure(1); clf; hold on;
plot(t, y);
plot(t, sum(v_fit, 2)/si(2));
shg
%% Panels for degenerate intensity fits and their spatio-temporal evolution
x_seed = [1.4*0.0378   1.96*19.8435];
[~, v_fit, ~, ~] = to_minimize(i_exp, x_seed, model, norm_fac);
figure; 
plot(v_fit(1:4:end, :)', 'Color', blue, 'LineWidth', 2);
make_fig_pretty('x (pxls)', 'intensity (a.u.)');
print([fig_fol, 'SpatialAnalysisAdvantages/fast_diff'], '-depsc');
figure; hold on;
plot(sum(v_fit'/si_v(2)), 'LineWidth', 3, 'Color', blue);
% plot(sum(r_n')/si_v(2), 'LineWidth', 3); 
x_seed = [1.4*0.5302    1.96*1.6535];
[~, v_fit, ~, ~] = to_minimize(i_exp, x_seed, model, norm_fac);
plot(sum(v_fit'/si_v(2)), 'LineWidth', 3, 'Color', red);
make_fig_pretty('time (s)', 'intensity (a.u.)');
ylim([0, 1]);
print([fig_fol, 'SpatialAnalysisAdvantages/intensity_comparison'], '-depsc');
figure; 
plot(v_fit(1:4:end, :)', 'Color', red, 'LineWidth', 2);
make_fig_pretty('x (pxls)', 'intensity (a.u.)');
print([fig_fol, 'SpatialAnalysisAdvantages/slow_diff'], '-depsc');
%% Panels showing the degeneracies with respect to real experimental data.
x_seed = [0.2376    4.0675];
[~, v_fit, ~, ~] = to_minimize(i_exp, x_seed, model, norm_fac);
figure; hold on; ylim([0, 1]);
plot(r_n(1:10:end, :)', 'LineWidth', 1.5, 'Color', [0.5, 0.5, 0.5]); 
plot(v_fit(1:10:end, :)', '--', 'LineWidth', 1.5, 'Color', blue)
make_fig_pretty('x (pxls)', 'intensity (a.u.)');
print([fig_fol, 'SpatialAnalysisAdvantages/wrong'], '-depsc');

figure; hold on;
plot(sum(r_n')/si_v(2), 'LineWidth', 3, 'Color', [0.5, 0.5, 0.5]); 
plot(sum(v_fit'/si_v(2)), '--', 'LineWidth', 3, 'Color', blue);
x_seed = [0.6930    1.6109];
[~, v_fit, ~, ~] = to_minimize(i_exp, x_seed, model, norm_fac);
plot(sum(v_fit'/si_v(2)), '--', 'LineWidth', 3, 'Color', red);
make_fig_pretty('time (s)', 'intensity (a.u.)');
ylim([0, 1]);
print([fig_fol, 'SpatialAnalysisAdvantages/intensity_comparison_data'], '-depsc');

figure; hold on; ylim([0, 1]);
plot(r_n(1:10:end, :)', 'b', 'LineWidth', 1.5, 'Color', [0.5, 0.5, 0.5]); 
plot(v_fit(1:10:end, :)', 'r--', 'LineWidth', 1.5, 'Color', red)
make_fig_pretty('x (pxls)', 'intensity (a.u.)');
print([fig_fol, 'SpatialAnalysisAdvantages/better'], '-depsc');

%% Double exponential fit:
y_d = sum(r_n');
x_d = time_step*(1:length(y_d));
fi = @(A1, A2, tau1, tau2, x) (A1+A2) - A1*exp(-x/tau1) - A2*exp(-x/tau2);
f = fit(x_d', y_d', fi,...
        'StartPoint',  [max(y_d)/2, max(y_d)/2, max(x_d)/2, max(x_d)/2]);
figure; hold on; 
plot(x_d, y_d, 'LineWidth', 3);
plot(x_d, f(x_d), '--', 'LineWidth', 3);
make_fig_pretty('time (s)', 'intensity (a.u.)');

%%
c = @(t, k) (t.^(k-1))./((1+t).^(k+1));
t = linspace(0, 20, 100);
figure; hold on;

for N = [10, 150, 180]
cs = zeros(N, length(t));
    for i = 1:N
        cs(i, :) = i*c(t, i);
    end
plot(t, nansum(cs));
end

%% Diffusion dependent on aggregate mass:
M = 1:20;
D = M.^(-1/3);
plot(M, D);
t_scaling = 1./sqrt(D);
figure;
plot(t_scaling)
t = repmat(linspace(0, 1, 20), 20);
t_all = (t.*t_scaling')';
t_all = t_all(:)';
[s, indx] = sort(t_all);

t = linspace(0, 1, 40);

%% Read simulation from python to compare my fitting with traditional way
i_exp = dlmread(['~/ownCloud/Dropbox_Lars_Christoph/DropletFRAP/',...
                     'PSFBleach/sim_spatial_prof.txt']);
x_seed = [0.984893867398430,2.01292973827329]; model = 'simple_drop';
time_step = 1;
r_tot = 0.997; % Radius given for droplet in FEM simul., check this is true!
pixel_size = r_tot/si(2);
si = size(i_exp);
norm_fac = mean(625/mean(i_exp(end, :)));
%%
function make_fig_pretty(xlab, ylab)
    g = gca;
    g.FontSize = 16;
    xlabel(xlab);
    ylabel(ylab);
end